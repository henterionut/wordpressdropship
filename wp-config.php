<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'a6u1?C_&Kt?Uy0Qp,b}y:W+aOmQqYl1bMB 8@`axqh:-1w3 m521EjrVydlcJQXj');
define('SECURE_AUTH_KEY',  'x)8lr}^|0({;bt=1HN.-M$.D,cwZXgP^izxkKJ|NrdL?lJO%Dyu_$m`sqFI7U&w.');
define('LOGGED_IN_KEY',    '6^@W;$O4p^+rt.GN#qpqb9(A1]&<RC?(,y*/<na3%C/ukd[k5Ec2W{! Ed$K@N]k');
define('NONCE_KEY',        ',rGy#4ckt>r!Bu^LKb.iegPQN-,m.-pbJpzsdW5.?q2#qkYo(nc4(3h)<?%fH$:-');
define('AUTH_SALT',        ';TPUZ4. J:-8p>z736,v%.eqm/,x9U^fMz0wn+|z4)qMn>zX09=gEY4=JuO4h@ q');
define('SECURE_AUTH_SALT', 'm19~[{KBXc_(0`ppJY*zqCN=v*^>7ZO}^-e8|4i2XlJd{8!g<nTj6++[6>H5Ywyq');
define('LOGGED_IN_SALT',   '/Xmym[5j-{h#!~tZhg.D(MP2N_J7XMp|`*U=?fb2Id+c`|4cz|^,#,s{V5>tQPM[');
define('NONCE_SALT',       'zPIT!>R(i|&UXpJq,>{xVMs|4N=IlD`Pv6T=(]ae k35q6{X+Ewr{0K6[o:0M%bx');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
